﻿namespace Codefarts.GMGProceduralBlockPlugin
{
    public sealed class SettingKeys
    {
        public const string MinUVOffset = "ProceduralBlockTool/MinUVOffsetValue";
        public const string MaxUVOffset = "ProceduralBlockTool/MaxUVOffsetValue";
        public const string MaxUVScale = "ProceduralBlockTool/MaxUVScaleValue";
    }
}
