namespace Codefarts.GMGProceduralBlockPlugin
{
    using System;

    using Codefarts.CoreProjectCode.Settings;
    using Codefarts.GridMapGame.Code.MapEditor;
    using Codefarts.GridMapGame.EditorTools;
    using Codefarts.GridMapGame.Scripts;
    using Codefarts.UIControls;
    using Codefarts.UIControls.Unity;

    using UnityEngine;

    public partial class ProceduralBlockTool
    {
        private AssetBrowserControl assetBrowser;

        private Button toolBarButton;

        private StackPanel mainContainer;

        private ButtonCheckBox showMouseOver;

        private StackPanel toolBar;

        private Button floorButton;
        private Button wallsButton;
        private Button ceilingButton;

        private Button updateButton;

        private Button floorObjectButton;

        private Button ceilingObjectButton;

        private SliderTextBoxControl topSlider;

        private SliderTextBoxControl bottomSlider;

        private ButtonCheckBox autoDraw;

        private Button updateTopButton;
        private Button updateBottomButton;

        private Button clearSelections;

        private Button deleteButton;

        private ButtonCheckBox textureCoordsToggle;

        private StackPanel mainPropertiesContainer;

        private StackPanel mainTextueContainer;

        private SliderTextBoxControl wallUOffset;

        private SliderTextBoxControl wallVOffset;

        private SliderTextBoxControl wallUScale;

        private SliderTextBoxControl wallVScale;

        private SliderTextBoxControl floorUOffset;

        private SliderTextBoxControl floorVOffset;

        private SliderTextBoxControl floorUScale;

        private SliderTextBoxControl floorVScale;

        /// <summary>
        /// The ceilings u offset.
        /// </summary>
        private SliderTextBoxControl ceilingUOffset;

        private SliderTextBoxControl ceilingVOffset;

        private SliderTextBoxControl ceilingUScale;

        private SliderTextBoxControl ceilingVScale;

        private Button wallUVOffsetUpdate;

        private Button wallUVScaleUpdate;

        private Button floorUVOffsetUpdate;

        private Button floorUVScaleUpdate;

        private Button ceilingUVOffsetUpdate;

        private Button ceilingUVScaleUpdate;

        private Button defaultTextureUV;

        private CheckBox liveWallUVOffsetUpdate;

        private CheckBox liveWallUVScaleUpdate;

        private CheckBox liveFloorUVOffsetUpdate;

        private CheckBox liveFloorUVScaleUpdate;

        private CheckBox liveCeilingUVOffsetUpdate;

        private CheckBox liveCeilingUVScaleUpdate;

        private CheckBox liveUpdateAll;

        private void SetupUIControls()
        {
            var settings = GameObject.FindObjectOfType<SettingsMonoBehaviour>();
            this.assetBrowser = new AssetBrowserControl();
            this.assetBrowser.Path = Application.dataPath;
            this.assetBrowser.Extensions.AddRange(new[] { ".png", ".jpg", ".jpeg" });
            this.assetBrowser.TexturePreviewSize = settings.Settings.GetSetting(GridMapGame.Code.SettingKeys.TexturePreviewSize, 64);

            this.toolBar = new StackPanel(Orientation.Horizontial);

            var objectInfoIcon = settings.Settings.GetSetting("Icons/ObjectInformation", GridMapGame.Code.SettingKeys.NoIconTexture);
            this.showMouseOver = new ButtonCheckBox(new Texture2DSource(Resources.Load<Texture2D>(objectInfoIcon)));

            var autoDrawIcon = settings.Settings.GetSetting("Icons/AutoDraw", GridMapGame.Code.SettingKeys.NoIconTexture);
            this.autoDraw = new ButtonCheckBox(new Texture2DSource(Resources.Load<Texture2D>(autoDrawIcon)));

            var textureIcon = settings.Settings.GetSetting("Icons/TextureCoords", GridMapGame.Code.SettingKeys.NoIconTexture);
            this.textureCoordsToggle = new ButtonCheckBox(new Texture2DSource(Resources.Load<Texture2D>(textureIcon)));
            this.textureCoordsToggle.Checked += this.textureCoordsToggle_Checked;

            var selectionButtons = new StackPanel(Orientation.Horizontial);
            this.clearSelections = new Button("Clear Selections");
            this.deleteButton = new Button("Delete");
            selectionButtons.Children.Add(this.clearSelections);
            selectionButtons.Children.Add(this.deleteButton);

            this.clearSelections.Click += this.clearSelections_Click;
            this.deleteButton.Click += this.deleteButton_Click;

            var updateButtons = new StackPanel(Orientation.Vertical);
            this.updateTopButton = new Button("Update Top");
            this.updateBottomButton = new Button("Update Bottom");
            updateButtons.Children.Add(this.updateTopButton);
            updateButtons.Children.Add(this.updateBottomButton);

            this.updateTopButton.Click += this.UpdateTopButtonClick;
            this.updateBottomButton.Click += this.UpdateBottomButtonClick;

            var blockProperties = new StackPanel(Orientation.Vertical);
            this.topSlider = new SliderTextBoxControl("Top", 1, -15, 15) { Width = 64, Precision = 2 };
            this.bottomSlider = new SliderTextBoxControl("Bottom", 0, -15, 15) { Width = 64, Precision = 2 };
            blockProperties.Children.Add(this.topSlider);
            blockProperties.Children.Add(this.bottomSlider);

            this.topSlider.ValueChanged += this.OnTopSliderValueChanged;
            this.bottomSlider.ValueChanged += this.OnBottomSliderValueChanged;

            var propertiesContainer = new StackPanel(Orientation.Horizontial);
            propertiesContainer.Children.Add(blockProperties);
            propertiesContainer.Children.Add(updateButtons);

            var materialsContainer = new StackPanel(Orientation.Horizontial);
            var shapesContainer = new StackPanel(Orientation.Horizontial);
            var materialsAndShapesContainer = new StackPanel(Orientation.Vertical);
            materialsAndShapesContainer.Children.Add(materialsContainer);
            materialsAndShapesContainer.Children.Add(shapesContainer);

            var floorButtonContainer = new StackPanel(Orientation.Vertical);
            var wallsButtonContainer = new StackPanel(Orientation.Vertical);
            var ceilingButtonContainer = new StackPanel(Orientation.Vertical);
            var clearFloorHeaderButton = new Button("Floor");
            var clearWallsHeaderButton = new Button("Walls");
            var clearCeilingHeaderButton = new Button("Ceiling");

            clearFloorHeaderButton.Click += (s, e) =>
                {
                    this.floorButton.Tag = null;
                    this.floorButton.Image = null;
                };
            clearWallsHeaderButton.Click += (s, e) =>
                {
                    this.wallsButton.Tag = null;
                    this.wallsButton.Image = null;
                };
            clearCeilingHeaderButton.Click += (s, e) =>
                {
                    this.ceilingButton.Tag = null;
                    this.ceilingButton.Image = null;
                };

            this.floorButton = new Button() { Width = 64, Height = 64 };
            this.wallsButton = new Button() { Width = 64, Height = 64 };
            this.ceilingButton = new Button() { Width = 64, Height = 64 };

            floorButtonContainer.Children.Add(clearFloorHeaderButton);
            floorButtonContainer.Children.Add(this.floorButton);

            wallsButtonContainer.Children.Add(clearWallsHeaderButton);
            wallsButtonContainer.Children.Add(this.wallsButton);

            ceilingButtonContainer.Children.Add(clearCeilingHeaderButton);
            ceilingButtonContainer.Children.Add(this.ceilingButton);

            materialsContainer.Children.Add(new FlexibleSpace());
            materialsContainer.Children.Add(floorButtonContainer);
            materialsContainer.Children.Add(new FlexibleSpace());
            materialsContainer.Children.Add(wallsButtonContainer);
            materialsContainer.Children.Add(new FlexibleSpace());
            materialsContainer.Children.Add(ceilingButtonContainer);
            materialsContainer.Children.Add(new FlexibleSpace());

            this.floorButton.Click += this.SetMaterialOrShapeButtonClick;
            this.wallsButton.Click += this.SetMaterialOrShapeButtonClick;
            this.ceilingButton.Click += this.SetMaterialOrShapeButtonClick;

            var floorObjectButtonContainer = new StackPanel(Orientation.Vertical);
            var ceilingObjectButtonContainer = new StackPanel(Orientation.Vertical);
            var clearObjectFloorHeaderButton = new Button("Floor");
            var clearObjectCeilingHeaderButton = new Button("Ceiling");

            this.floorObjectButton = new Button() { Width = 64, Height = 64 };
            this.ceilingObjectButton = new Button() { Width = 64, Height = 64 };

            this.floorObjectButton.Click += this.SetMaterialOrShapeButtonClick;
            this.ceilingObjectButton.Click += this.SetMaterialOrShapeButtonClick;
            clearObjectFloorHeaderButton.Click += (s, e) =>
                {
                    this.ceilingObjectButton.Tag = null;
                    this.ceilingObjectButton.Image = null;
                };
            clearObjectCeilingHeaderButton.Click += (s, e) =>
                {
                    this.floorObjectButton.Tag = null;
                    this.floorObjectButton.Image = null;
                };

            floorObjectButtonContainer.Children.Add(clearObjectFloorHeaderButton);
            floorObjectButtonContainer.Children.Add(this.floorObjectButton);

            ceilingObjectButtonContainer.Children.Add(clearObjectCeilingHeaderButton);
            ceilingObjectButtonContainer.Children.Add(this.ceilingObjectButton);

            shapesContainer.Children.Add(new FlexibleSpace());
            shapesContainer.Children.Add(floorObjectButtonContainer);
            shapesContainer.Children.Add(new FlexibleSpace());
            shapesContainer.Children.Add(ceilingObjectButtonContainer);
            shapesContainer.Children.Add(new FlexibleSpace());

            this.updateButton = new Button("Update");
            this.updateButton.Click += this.UpdateButtonClick;

            this.mainContainer = new StackPanel(Orientation.Vertical) { IsEnabled = false };
            this.mainContainer.Children.Add(this.toolBar);
            this.toolBar.Children.Add(this.autoDraw);
            this.toolBar.Children.Add(this.showMouseOver);
            this.toolBar.Children.Add(this.textureCoordsToggle);

            this.mainPropertiesContainer = new StackPanel(Orientation.Vertical);
            this.mainPropertiesContainer.Children.Add(this.assetBrowser);
            this.mainPropertiesContainer.Children.Add(selectionButtons);
            this.mainPropertiesContainer.Children.Add(propertiesContainer);
            this.mainPropertiesContainer.Children.Add(materialsAndShapesContainer);
            this.mainPropertiesContainer.Children.Add(this.updateButton);

            this.mainTextueContainer = new StackPanel(Orientation.Vertical) { Visibility = Visibility.Collapsed };

            var minOffset = settings.Settings.GetSetting(SettingKeys.MinUVOffset, -1f);
            var maxOffset = settings.Settings.GetSetting(SettingKeys.MaxUVOffset, 1f);
            var maxScale = settings.Settings.GetSetting(SettingKeys.MaxUVScale, 1000f);
            var offsetMidPoint = Math.Min(minOffset, maxOffset) + ((Math.Max(minOffset, maxOffset) - Math.Min(minOffset, maxOffset)) / 2);

            var sliderContainer = new StackPanel(Orientation.Horizontial);
            this.wallUOffset = new SliderTextBoxControl("Wall Offset U", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.wallVOffset = new SliderTextBoxControl("Wall Offset V", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.wallUOffset.ValueChanged += this.WallUvOffsetValueChanged;
            this.wallVOffset.ValueChanged += this.WallUvOffsetValueChanged;
            var updateContainer = new StackPanel(Orientation.Vertical);
            this.wallUVOffsetUpdate = new Button("Update");
            this.liveWallUVOffsetUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveWallUVOffsetUpdate);
            updateContainer.Children.Add(this.wallUVOffsetUpdate);
            this.wallUVOffsetUpdate.Click += this.UpdateWallUvOffsetClick;
            sliderContainer.Children.Add(this.wallUOffset);
            sliderContainer.Children.Add(this.wallVOffset);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            sliderContainer = new StackPanel(Orientation.Horizontial);
            this.wallUScale = new SliderTextBoxControl("Wall Scale U", 1, 0, maxScale) { Precision = 3 };
            this.wallVScale = new SliderTextBoxControl("Wall Scale V", 1, 0, maxScale) { Precision = 3 };
            this.wallUScale.ValueChanged += this.WallUvScaleValueChanged;
            this.wallVScale.ValueChanged += this.WallUvScaleValueChanged;
            this.wallUVScaleUpdate = new Button("Update");
            updateContainer = new StackPanel(Orientation.Vertical);
            this.liveWallUVScaleUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveWallUVScaleUpdate);
            updateContainer.Children.Add(this.wallUVScaleUpdate);
            this.wallUVScaleUpdate.Click += this.UpdateWallUvScaleClick;
            sliderContainer.Children.Add(this.wallUScale);
            sliderContainer.Children.Add(this.wallVScale);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            sliderContainer = new StackPanel(Orientation.Horizontial);
            this.floorUOffset = new SliderTextBoxControl("Floor Offset U", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.floorVOffset = new SliderTextBoxControl("Floor Offset V", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.floorUOffset.ValueChanged += this.FloorUvOffsetValueChanged;
            this.floorVOffset.ValueChanged += this.FloorUvOffsetValueChanged;
            this.floorUVOffsetUpdate = new Button("Update");
            updateContainer = new StackPanel(Orientation.Vertical);
            this.liveFloorUVOffsetUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveFloorUVOffsetUpdate);
            updateContainer.Children.Add(this.floorUVOffsetUpdate);
            this.floorUVOffsetUpdate.Click += this.UpdateFloorUvOffsetClick;
            sliderContainer.Children.Add(this.floorUOffset);
            sliderContainer.Children.Add(this.floorVOffset);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            sliderContainer = new StackPanel(Orientation.Horizontial);
            this.floorUScale = new SliderTextBoxControl("Floor Scale U", 1, 0, maxScale) { Precision = 3 };
            this.floorVScale = new SliderTextBoxControl("Floor Scale V", 1, 0, maxScale) { Precision = 3 };
            this.floorUScale.ValueChanged += this.FloorUvScaleValueChanged;
            this.floorVScale.ValueChanged += this.FloorUvScaleValueChanged;
            this.floorUVScaleUpdate = new Button("Update");
            updateContainer = new StackPanel(Orientation.Vertical);
            this.liveFloorUVScaleUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveFloorUVScaleUpdate);
            updateContainer.Children.Add(this.floorUVScaleUpdate);
            this.floorUVScaleUpdate.Click += this.UpdateFloorUvScaleClick;
            sliderContainer.Children.Add(this.floorUScale);
            sliderContainer.Children.Add(this.floorVScale);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            sliderContainer = new StackPanel(Orientation.Horizontial);
            this.ceilingUOffset = new SliderTextBoxControl("Ceiling Offset U", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.ceilingVOffset = new SliderTextBoxControl("Ceiling Offset V", offsetMidPoint, minOffset, maxOffset) { Precision = 3 };
            this.ceilingUOffset.ValueChanged += this.CeilingUvOffsetValueChanged;
            this.ceilingVOffset.ValueChanged += this.CeilingUvOffsetValueChanged;
            this.ceilingUVOffsetUpdate = new Button("Update");
            updateContainer = new StackPanel(Orientation.Vertical);
            this.liveCeilingUVOffsetUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveCeilingUVOffsetUpdate);
            updateContainer.Children.Add(this.ceilingUVOffsetUpdate);
            this.ceilingUVOffsetUpdate.Click += this.UpdateCeilingUvOffsetClick;
            sliderContainer.Children.Add(this.ceilingUOffset);
            sliderContainer.Children.Add(this.ceilingVOffset);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            sliderContainer = new StackPanel(Orientation.Horizontial);
            this.ceilingUScale = new SliderTextBoxControl("Ceiling Scale U", 1, 0, maxScale) { Precision = 3 };
            this.ceilingVScale = new SliderTextBoxControl("Ceiling Scale V", 1, 0, maxScale) { Precision = 3 };
            this.ceilingUScale.ValueChanged += this.CeilingUvScaleValueChanged;
            this.ceilingVScale.ValueChanged += this.CeilingUvScaleValueChanged;
            this.ceilingUVScaleUpdate = new Button("Update");
            updateContainer = new StackPanel(Orientation.Vertical);
            this.liveCeilingUVScaleUpdate = new CheckBox("Live");
            updateContainer.Children.Add(this.liveCeilingUVScaleUpdate);
            updateContainer.Children.Add(this.ceilingUVScaleUpdate);
            this.ceilingUVScaleUpdate.Click += this.UpdateCeilingUvScaleClick;
            sliderContainer.Children.Add(this.ceilingUScale);
            sliderContainer.Children.Add(this.ceilingVScale);
            sliderContainer.Children.Add(updateContainer);
            this.mainTextueContainer.Children.Add(sliderContainer);

            this.mainTextueContainer.Children.Add(new FlexibleSpace());

            this.liveUpdateAll = new CheckBox("Live Update All");
            this.liveUpdateAll.Checked += this.LiveUpdateAllChecked;
            this.mainTextueContainer.Children.Add(this.liveUpdateAll);

            this.defaultTextureUV = new Button("Defaults");
            this.defaultTextureUV.Click += this.SetDefaultTextureUvClick;
            this.mainTextueContainer.Children.Add(this.defaultTextureUV);

            this.mainContainer.Children.Add(this.mainPropertiesContainer);
            this.mainContainer.Children.Add(this.mainTextueContainer);

            var toolBarIcon = settings.Settings.GetSetting("Icons/ProceduralBlock", GridMapGame.Code.SettingKeys.NoIconTexture);
            this.toolBarButton = new Button() { Image = new Texture2DSource(Resources.Load<Texture2D>(toolBarIcon)) };
            this.toolBarButton.MaxHeight = editor.Toolbar.MaxHeight;
            this.toolBarButton.MaxWidth = editor.Toolbar.MaxHeight;
            this.toolBarButton.Click += this.ToolBarButtonClick;
            this.editor.Toolbar.Children.Add(this.toolBarButton);
        }
    }
}