﻿namespace Codefarts.GMGProceduralBlockPlugin
{
    using System;

    using Codefarts.CoreProjectCode.Settings;
    using Codefarts.GeneralTools.Code;
    using Codefarts.GeneralTools.Models;
    using Codefarts.GeneralTools.Scripts;
    using Codefarts.GMGProceduralBlockPlugin.Models;
    using Codefarts.GridMapGame;
    using Codefarts.GridMapGame.Models;
    using Codefarts.GridMapGame.Scripts;
    using Codefarts.GridMaps.Scripts;
    using Codefarts.MeshMaps;
    using Codefarts.MeshMaps.Scripts;
    using Codefarts.UIControls;
    using Codefarts.UIControls.Unity;
                                
    using UnityEngine;

    /// <summary>
    /// Provides tools for saving & loading maps.
    /// </summary>
    public partial class ProceduralBlockTool : IEditorTool<MapEditorScreen>
    {
        private const string BlockToolCategory = "Tools/Procedural Block";

        /// <summary>
        /// Holds the editor reference passed into the <see cref="Startup"/> method.
        /// </summary>
        private MapEditorScreen editor;

        /// <summary>
        /// Stores the value for the <see cref="Information"/> property.
        /// </summary>
        private readonly PluginInformation pluginInformation;

        private IMapObjectVertexData<Guid> mouseOverObject;
                                       
        private Vector3 mouseHitPos;

        private bool activeTool;

        private GridMapManagerBase currentMapManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProceduralBlockTool"/> class.
        /// </summary>
        public ProceduralBlockTool()
        {
            this.pluginInformation = new PluginInformation()
                {
                    Title = "Block",
                    Uid = new Guid("26D36EEE-AA78-4AE7-BB1C-AC96D7468720"),
                    //   Content = new[] { new PluginInformationContent("Block", Resources.Load<Texture2D>("MageStorm/Icons/ProceduralBlock")) }
                };
        }

        public void DrawSettings(string category)
        {
            switch (category)
            {
                case BlockToolCategory:
                    GUILayout.BeginVertical();
                    GUILayout.Label("Settings for procedural block tool go here!");
                    GUILayout.EndVertical();
                    break;
            }
        }

        public string[] SettingCategories
        {
            get
            {
                return new[] { BlockToolCategory };
            }
        }

        /// <summary>
        /// Gets the information for the plugin.
        /// </summary>   
        public PluginInformation Information
        {
            get
            {
                return new PluginInformation(this.pluginInformation);
            }
        }

        private void clearSelections_Click(object sender, System.EventArgs e)
        {
            SelectionManager.Instance.Clear();
        }

        /// <summary>
        /// Method will be called when the tool is no longer of use by the system or a new tool is being selected.
        /// </summary>                                                                                                                       
        public void Shutdown()
        {
            this.Deactivate();
            this.editor.Map.Status -= this.Map_Status;
            SelectionManager.Instance.Status -= this.SelectionManagerStatus;
            this.editor.Toolbar.Children.Remove(this.toolBarButton);
            this.toolBarButton.Click -= this.ToolBarButtonClick;
            this.toolBarButton = null;
            this.mainContainer = null;
            this.editor = null;
        }

        /// <summary>
        /// Called when the tool is set as the active tool.
        /// </summary>
        /// <param name="editor">A reference to the editor that this tool should work against.</param>
        /// <exception cref="System.InvalidOperationException">Startup already called.</exception>
        public void Startup(MapEditorScreen editor)
        {
            if (this.editor != null)
            {
                throw new InvalidOperationException("Startup already called.");
            }

            this.editor = editor;
            this.SetupUIControls();

            SelectionManager.Instance.Status += this.SelectionManagerStatus;
            this.editor.Map.Status += this.Map_Status;

            var mapObject = this.editor.Map.CurrentMap;
            if (mapObject == null)
            {
                return;
            }

            var map = mapObject.GetComponent<BasicGridMap>();
            if (map == null)
            {
                return;
            }

            this.currentMapManager = map.GetComponent<GridMapManagerBase>();
        }

        private void Map_Status(object sender, MapLoadingArgs e)
        {
            if (e.State == ProgressState.Completed)
            {
                this.currentMapManager = e.Map.GetComponent<GridMapManagerBase>();
                this.mainContainer.IsEnabled = this.currentMapManager != null;
            }
        }

        private void textureCoordsToggle_Checked(object sender, System.EventArgs e)
        {
            this.mainTextueContainer.Visibility = this.textureCoordsToggle.IsChecked ? Visibility.Visible : Visibility.Collapsed;
            this.mainPropertiesContainer.Visibility = !this.textureCoordsToggle.IsChecked ? Visibility.Visible : Visibility.Collapsed;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            this.DeleteSelections();
        }

        private void SelectionManagerStatus(object sender, SelectionArgs e)
        {
            if (!this.activeTool)
            {
                return;
            }

            switch (e.Action)
            {
                case SelectionAction.Added:
                    var hasPoints = false;
                    foreach (var item in e.NewItems)
                    {
                        if (item is Point2D)
                        {
                            hasPoints = true;
                            break;
                        }
                    }

                    if (this.autoDraw.IsChecked && hasPoints)
                    {
                        this.CreateBlocks();
                        SelectionManager.Instance.Clear();
                    }

                    break;

                case SelectionAction.Removed:
                    break;
                case SelectionAction.Changed:
                    break;
                case SelectionAction.Cleared:
                    break;
            }
        }

        private void UpdateTopButtonClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            foreach (var block in selections)
            {
                block.FloorTop = this.topSlider.Value;
            }
        }

        private void UpdateBottomButtonClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            foreach (var block in selections)
            {
                block.FloorBottom = this.bottomSlider.Value;
            }
        }

        private void UpdateButtonClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            foreach (var block in selections)
            {
                this.AssignBlockMaterials(block);
            }
        }

        private void SetMaterialOrShapeButtonClick(object sender, EventArgs e)
        {
            var button = (Button)sender;
            button.Tag = this.assetBrowser.SelectedFile;
            if (button.Image is Texture2DSource)
            {
                var source = button.Image as Texture2DSource;
                source.Texture = this.assetBrowser.GetSelectedPreview();
            }
            else
            {
                button.Image = new Texture2DSource(this.assetBrowser.GetSelectedPreview());
            }
        }

        private void ToolBarButtonClick(object sender, EventArgs e)
        {
            this.editor.Plugins.SetActivePlugin(this.pluginInformation.Uid);
        }

        /// <summary>
        /// Called by the editor to update the tool.
        /// </summary>
        public void Update(float elapsedGameTime, float totalGameTime)
        {
            if (!this.activeTool || this.currentMapManager == null)
            {
                return;
            }

            // check to delete deletions
            if (Input.GetKeyUp(KeyCode.Delete))
            {
                this.DeleteSelections();
            }

            if (Input.GetKeyUp(KeyCode.C))
            {
                this.CreateBlocks();
            }
        }

        public void DeleteSelections()
        {
            var selectionManager = SelectionManager.Instance;
            var selections = selectionManager.GetSelections<IMapObjectVertexData<Guid>>();

            foreach (var item in selections)
            {
                if (item is BlockModel)
                {
                    var block = (BlockModel)item;
                    var stack = this.currentMapManager.Get(block.GridLocationX, block.GridLocationY);
                    var index = Array.IndexOf(stack, block);
                    if (index != -1)
                    {
                        this.currentMapManager.Remove(block.GridLocationX, block.GridLocationY, index);
                    }
                }
            }

            SelectionManager.Instance.Clear();
        }

        private void OnBottomSliderValueChanged(object s, RoutedPropertyChangedEventArgs<float> e)
        {
            this.topSlider.Value = this.bottomSlider.Value > this.topSlider.Value ? this.bottomSlider.Value : this.topSlider.Value;
        }

        private void OnTopSliderValueChanged(object s, RoutedPropertyChangedEventArgs<float> e)
        {
            this.bottomSlider.Value = this.topSlider.Value < this.bottomSlider.Value ? this.topSlider.Value : this.bottomSlider.Value;
        }

        public void CreateBlocks()
        {
            var selectionManager = SelectionManager.Instance;
            var selections = selectionManager.GetSelections<Point2D>();

            foreach (var selection in selections)
            {
                var newBlockModel = new BlockModel();
                newBlockModel.GridLocationX = selection.X;
                newBlockModel.GridLocationY = selection.Y;

                //  Debug.Log(string.Format("{0}x{1}", newBlockModel.LocationX, newBlockModel.LocationY));

                newBlockModel.SetProperties(this.topSlider.Value, this.bottomSlider.Value);
                newBlockModel.SetCeilingUv(
                    this.ceilingUOffset.Value,
                    this.ceilingVOffset.Value,
                    this.ceilingUScale.Value,
                    this.ceilingVScale.Value);

                newBlockModel.SetFloorUv(
                    this.floorUOffset.Value,
                    this.floorVOffset.Value,
                    this.floorUScale.Value,
                    this.floorVScale.Value);

                newBlockModel.SetWallUv(
                    this.wallUOffset.Value,
                    this.wallVOffset.Value,
                    this.wallUScale.Value,
                    this.wallVScale.Value);

                /*
                // if a ceiling object is available clone it and assign it to the block
                if (this.ceilingObject != null)
                {
                    newBlockModel.CeilingShape = GameObject.Instantiate(this.ceilingObject) as GameObject;
                    newBlockModel.CeilingShape.SetActive(true);
                }

                // if a floor object is available clone it and assign it to the block
                if (this.floorObject != null)
                {
                    newBlockModel.FloorShape = GameObject.Instantiate(this.floorObject) as GameObject;
                    newBlockModel.FloorShape.SetActive(true);
                } */

                this.AssignBlockMaterials(newBlockModel);
                this.currentMapManager.Add(selection.X, selection.Y, newBlockModel);

                this.PositionShapes(newBlockModel);
                this.UpdateShapeRotations(newBlockModel);
                this.ScaleShape(newBlockModel.FloorShape);
                this.ScaleShape(newBlockModel.CeilingShape);
            }

            SelectionManager.Instance.Clear();
        }

        private void AssignBlockMaterials(BlockModel block)
        {
            var wallMaterial = PrefabContainer.Instance.GetPrefab("DefaultWallMaterial") as Material;
            var floorMaterial = PrefabContainer.Instance.GetPrefab("DefaultFloorMaterial") as Material;
            var ceilingMaterial = PrefabContainer.Instance.GetPrefab("DefaultCeilingMaterial") as Material;
            block.Walls = this.wallsButton.Tag == null
                        ? wallMaterial
                        : this.editor.Map.ContentManager.Load<Material>((string)this.wallsButton.Tag);
            block.Ceiling = this.ceilingButton.Tag == null
                          ? ceilingMaterial
                          : this.editor.Map.ContentManager.Load<Material>((string)this.ceilingButton.Tag);
            block.Floor = this.floorButton.Tag == null
                              ? floorMaterial
                              : this.editor.Map.ContentManager.Load<Material>((string)this.floorButton.Tag);

            block.FloorTexturePath = this.floorButton.Tag as string;
            block.WallTexturePath = this.wallsButton.Tag as string;
            block.CeilingTexturePath = this.ceilingButton.Tag as string;
        }

        private void PositionShapes(BlockModel model)
        {
            if (model == null)
            {
                return;
            }

            var shape = model.FloorShape;
            if (shape != null)
            {
                shape.transform.position = new Vector3(model.GridLocationX, model.FloorTop, model.GridLocationY);
            }

            shape = model.CeilingShape;
            if (shape != null)
            {
                shape.transform.position = new Vector3(model.GridLocationX, model.FloorBottom, model.GridLocationY);
            }
        }

        private void UpdateShapeRotations(BlockModel model)
        {
            if (model == null)
            {
                return;
            }

            var shape = model.FloorShape;
            if (shape != null)
            {
                var rotation = shape.transform.eulerAngles;
                rotation.y = model.FloorShapeRotation;
                shape.transform.eulerAngles = rotation;
            }

            shape = model.CeilingShape;
            if (shape != null)
            {
                var rotation = shape.transform.eulerAngles;
                rotation.y = model.CeilingShapeRotation;
                shape.transform.eulerAngles = rotation;
            }
        }

        private void ScaleShape(GameObject shape)
        {
            if (shape == null)
            {
                return;
            }

            var currentMap = this.editor.Map.CurrentMap;
            if (currentMap != null)
            {
                shape.transform.parent = currentMap.transform;
            }

            var meshRenderer = shape.GetComponent<MeshRenderer>();
            var scale = Vector3.one;
            if (meshRenderer != null)
            {
                var size = meshRenderer.bounds.size;
                var max = Math.Max(size.x, size.y);
                var scaling = 1 / max;
                scale = Vector3.one * scaling;
            }

            shape.transform.localScale = scale;
        }

        private void DrawMouseOverInformation()
        {
            if (!this.showMouseOver.IsChecked)
            {
                return;
            }

            var data = string.Empty;

            if (this.mouseOverObject is BlockModel)
            {
                var model = this.mouseOverObject as BlockModel;

                data += string.Format(
                    "\r\nType: Block ({0}x{1}) Layer: ?",
                    model.GridLocationX,
                    model.GridLocationY); //,
                // Enum.GetName(typeof(MapLayerIndex), i));
                data += string.Format(
                    "\r\nHeight: {0} Top: {1} Bottom: {2}",
                    Math.Round(model.FloorTop - model.FloorBottom, 2),
                    Math.Round(model.FloorTop, 2),
                    Math.Round(model.FloorBottom, 2));
            }

            data = data.Trim();
            if (!string.IsNullOrEmpty(data))
            {
                var textSize = GUI.skin.label.CalcSize(new GUIContent(data));
                var mousePosition = Input.mousePosition;
                var rect = new Rect(mousePosition.x + 16, -mousePosition.y + Screen.height - 10, textSize.x, textSize.y);
                GUI.Box(rect, string.Empty);
                GUI.Label(rect, data);
            }
        }

        /// <summary>
        /// Called by the editor to allow the tool to draw it self on the tool bar.
        /// </summary>
        /// <returns>
        /// If true the tool has drawn it self and no further drawing is necessary, otherwise false.
        /// </returns>
        public void DrawToolInterface(float elapsedGameTime, float totalGameTime)
        {
            if (this.editor == null)
            {
                throw new NullReferenceException("Startup not called!");
            }

            ControlRendererManager.Instance.DrawControl(this.mainContainer, Time.deltaTime, Time.realtimeSinceStartup);
        }

        public void Activate()
        {
            this.activeTool = true;
        }

        public void Deactivate()
        {
            this.activeTool = false;
        }

        /// <summary>
        /// Called by the editor to notify the tool to draw something.
        /// </summary>
        public void Draw(float elapsedGameTime, float totalGameTime)
        {
            if (!this.activeTool)
            {
                return;
            }

            this.DrawMouseOverInformation();
        }

        private void SetDefaultTextureUvClick(object sender, EventArgs e)
        {
            var settings = GameObject.FindObjectOfType<SettingsMonoBehaviour>();
            var minOffset = settings.Settings.GetSetting(SettingKeys.MinUVOffset, -1f);
            var maxOffset = settings.Settings.GetSetting(SettingKeys.MaxUVOffset, 1f);
            var offsetMidPoint = Math.Min(minOffset, maxOffset) + ((Math.Max(minOffset, maxOffset) - Math.Min(minOffset, maxOffset)) / 2);

            this.ceilingUOffset.Value = offsetMidPoint;
            this.ceilingVOffset.Value = offsetMidPoint;
            this.ceilingUScale.Value = 1;
            this.ceilingVScale.Value = 1;
            this.floorUOffset.Value = offsetMidPoint;
            this.floorVOffset.Value = offsetMidPoint;
            this.floorUScale.Value = 1;
            this.floorVScale.Value = 1;
            this.wallUOffset.Value = offsetMidPoint;
            this.wallVOffset.Value = offsetMidPoint;
            this.wallUScale.Value = 1;
            this.wallVScale.Value = 1;
        }

        private void UpdateCeilingUvScaleClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var scale = new Vector2(this.ceilingUScale.Value, this.ceilingVScale.Value);
            foreach (var block in selections)
            {
                block.CeilingUvScale = scale;
            }
        }

        private void UpdateCeilingUvOffsetClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var offset = new Vector2(this.ceilingUOffset.Value, this.ceilingVOffset.Value);
            foreach (var block in selections)
            {
                block.CeilingUvOffset = offset;
            }
        }

        private void UpdateFloorUvScaleClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var scale = new Vector2(this.floorUScale.Value, this.floorVScale.Value);
            foreach (var block in selections)
            {
                block.FloorUvScale = scale;
            }
        }

        private void UpdateFloorUvOffsetClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var offset = new Vector2(this.floorUOffset.Value, this.floorVOffset.Value);
            foreach (var block in selections)
            {
                block.FloorUvOffset = offset;
            }
        }

        private void UpdateWallUvScaleClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var scale = new Vector2(this.wallUScale.Value, this.wallVScale.Value);
            foreach (var block in selections)
            {
                block.WallUvScale = scale;
            }
        }

        private void UpdateWallUvOffsetClick(object sender, EventArgs e)
        {
            var selections = SelectionManager.Instance.GetSelections<BlockModel>();
            var offset = new Vector2(this.wallUOffset.Value, this.wallVOffset.Value);
            foreach (var block in selections)
            {
                block.WallUvOffset = offset;
            }
        }

        private void LiveUpdateAllChecked(object sender, EventArgs e)
        {
            var isNotChecked = !this.liveUpdateAll.IsChecked;
            this.liveCeilingUVOffsetUpdate.IsEnabled = isNotChecked;
            this.liveCeilingUVScaleUpdate.IsEnabled = isNotChecked;

            this.liveFloorUVOffsetUpdate.IsEnabled = isNotChecked;
            this.liveFloorUVScaleUpdate.IsEnabled = isNotChecked;

            this.liveWallUVOffsetUpdate.IsEnabled = isNotChecked;
            this.liveWallUVScaleUpdate.IsEnabled = isNotChecked;

            this.ceilingUVOffsetUpdate.IsEnabled = isNotChecked;
            this.ceilingUVScaleUpdate.IsEnabled = isNotChecked;

            this.floorUVOffsetUpdate.IsEnabled = isNotChecked;
            this.floorUVScaleUpdate.IsEnabled = isNotChecked;

            this.wallUVOffsetUpdate.IsEnabled = isNotChecked;
            this.wallUVScaleUpdate.IsEnabled = isNotChecked;
        }

        private void WallUvOffsetValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveWallUVOffsetUpdate.IsChecked)
            {
                this.UpdateWallUvOffsetClick(sender, e);
            }
        }

        private void WallUvScaleValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveWallUVScaleUpdate.IsChecked)
            {
                this.UpdateWallUvScaleClick(sender, e);
            }
        }

        private void FloorUvOffsetValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveFloorUVOffsetUpdate.IsChecked)
            {
                this.UpdateFloorUvOffsetClick(sender, e);
            }
        }

        private void FloorUvScaleValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveFloorUVScaleUpdate.IsChecked)
            {
                this.UpdateFloorUvScaleClick(sender, e);
            }
        }

        private void CeilingUvOffsetValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveCeilingUVOffsetUpdate.IsChecked)
            {
                this.UpdateCeilingUvOffsetClick(sender, e);
            }
        }

        private void CeilingUvScaleValueChanged(object sender, RoutedPropertyChangedEventArgs<float> e)
        {
            if (this.liveUpdateAll.IsChecked || this.liveCeilingUVScaleUpdate.IsChecked)
            {
                this.UpdateCeilingUvScaleClick(sender, e);
            }
        }
    }
}