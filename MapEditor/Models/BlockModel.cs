// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Codefarts" file="BlockModel.cs">
//   Copyright (c) 2012 Codefarts
//   //   All rights reserved.
//   //   contact@codefarts.com
//   //   http://www.codefarts.com
// </copyright>
// <summary>
//   Provides a data model that represents a block with materials for the top, bottom, and sides.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Codefarts.GMGProceduralBlockPlugin.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    using Codefarts.GridMapGame.Interfaces;
    using Codefarts.MeshMaps;

    using UnityEngine;

    /// <summary>
    /// Provides a data model that represents a block with materials for the top, bottom, and sides.
    /// </summary>
    [Serializable]
    public class BlockModel : INotifyPropertyChanged, INotifyPropertyChanging, IMapObjectVertexData<Guid>, ISerializable  ,IGridCellLocation
    {
        #region Fields

        /// <summary>
        /// Stores the path to the wall texture.
        /// </summary>
        [SerializeField]
        private string wallTexturePath;

        /// <summary>
        /// Stores the path to the ceiling texture.
        /// </summary>
        [SerializeField]
        private string ceilingTexturePath;

        /// <summary>
        /// Stores the path to the floor texture.
        /// </summary>
        [SerializeField]
        private string floorTexturePath;

        /// <summary>
        /// The bounds stores the bounds of the model.
        /// </summary>
        [NonSerialized]
        private Bounds bounds;

        /// <summary>
        /// The ceiling material.
        /// </summary>
        [SerializeField]
        private Material ceiling;

        /// <summary>
        /// The ceiling shape object.
        /// </summary>
        [SerializeField]
        private GameObject ceilingShape;

        /// <summary>
        /// The ceiling shape rotation value.
        /// </summary>
        [SerializeField]
        private float ceilingShapeRotation;

        /// <summary>
        /// The ceiling uv offset value.
        /// </summary>
        [SerializeField]
        private Vector2 ceilingUvOffset;

        /// <summary>
        /// The ceiling uv scale value. Default is (1,1).
        /// </summary>
        [SerializeField]
        private Vector2 ceilingUvScale = Vector2.one;

        /// <summary>
        /// Holds a cached collection of arguments to help improve performance.
        /// </summary>
        [NonSerialized]
        private Dictionary<string, PropertyChangedEventArgs> changedArgs = new Dictionary<string, PropertyChangedEventArgs>();

        /// <summary>
        /// Holds a cached collection of arguments to help improve performance.
        /// </summary>
        [NonSerialized]
        private Dictionary<string, PropertyChangingEventArgs> changingArgs = new Dictionary<string, PropertyChangingEventArgs>();

        /// <summary>
        /// The floor material.
        /// </summary>
        [SerializeField]
        private Material floor;

        /// <summary>
        /// The value for floor bottom.
        /// </summary>
        [SerializeField]
        private float floorBottom;

        /// <summary>
        /// The floor shape object...
        /// </summary>
        [SerializeField]
        private GameObject floorShape;

        /// <summary>
        /// The floor shape rotation value.
        /// </summary>
        [SerializeField]
        private float floorShapeRotation;

        /// <summary>
        /// The floor top value.
        /// </summary>
        [SerializeField]
        private float floorTop = 1;

        /// <summary>
        /// The floor uv offset.
        /// </summary>
        [SerializeField]
        private Vector2 floorUvOffset;

        /// <summary>
        /// The floor uv scale value. Default is (1,1).
        /// </summary>
        [SerializeField]
        private Vector2 floorUvScale = Vector2.one;

        /// <summary>
        /// The unique instance identifier for this model.
        /// </summary>
        [NonSerialized]
        private Guid instanceID = Guid.NewGuid();

        /// <summary>
        /// The x location in the map.
        /// </summary>
        [SerializeField]
        private int gridLocationX;

        /// <summary>
        /// The y location in the map.
        /// </summary>
        [SerializeField]
        private int gridLocationY;

        /// <summary>
        /// The vertex normals for the block model. Vertex normals are preset because they do not change.
        /// </summary>
        [NonSerialized]
        private Vector3[] vertexNormals = new[]
                {
                    new Vector3(0f, -1f, 0f), // bottom  0
                    new Vector3(0f, -1f, 0f), // bottom  1
                    new Vector3(0f, -1f, 0f), // bottom  2
                    new Vector3(0f, -1f, 0f), // bottom  3
                    new Vector3(0f, 1f, 0f), // top     4
                    new Vector3(0f, 1f, 0f), // top     5
                    new Vector3(0f, 1f, 0f), // top     6
                    new Vector3(0f, 1f, 0f), // top     7
                    new Vector3(0f, 0f, -1f), // back    8
                    new Vector3(0f, 0f, -1f), // back    9
                    new Vector3(0f, 0f, -1f), // back    10
                    new Vector3(0f, 0f, -1f), // back    11
                    new Vector3(-1f, 0f, 0f), // left    12
                    new Vector3(-1f, 0f, 0f), // left    13
                    new Vector3(-1f, 0f, 0f), // left    14
                    new Vector3(-1f, 0f, 0f), // left    15
                    new Vector3(0f, 0f, 1f), // front   16
                    new Vector3(0f, 0f, 1f), // front   17
                    new Vector3(0f, 0f, 1f), // front   18
                    new Vector3(0f, 0f, 1f), // front   19
                    new Vector3(1f, 0f, 0f), // right   20
                    new Vector3(1f, 0f, 0f), // right   21
                    new Vector3(1f, 0f, 0f), // right   22
                    new Vector3(1f, 0f, 0f) // right   23
                };

        /// <summary>
        /// The wall uv offset.
        /// </summary>
        [SerializeField]
        private Vector2 wallUvOffset;

        /// <summary>
        /// The wall uv scale value. Default is (1,1).
        /// </summary>
        [SerializeField]
        private Vector2 wallUvScale = Vector2.one;

        /// <summary>
        /// The wall material.
        /// </summary>
        [SerializeField]
        private Material walls;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BlockModel"/> class.
        /// </summary>
        public BlockModel()
        {
            // this.properties = new PropertyCollection();
            // this.properties["LocationX"] = this.locationX;
            // this.properties["LocationY"] = this.locationY;
            this.UpdateBounds();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlockModel"/> class.
        /// </summary>
        /// <param name="info">
        /// The serialization information.
        /// </param>
        /// <param name="context">
        /// The streaming context.
        /// </param>
        /// <remarks>
        /// The special constructor is used to deserialize values.
        /// </remarks>
        public BlockModel(SerializationInfo info, StreamingContext context)
            : this()
        {
            // Reset the property value using the GetValue method.
            this.CeilingShapeRotation = (float)info.GetValue("ceilingShapeRotation", typeof(float));
            this.CeilingUvOffset = (Vector2)info.GetValue("ceilingUvOffset", typeof(Vector2));
            this.CeilingUvScale = (Vector2)info.GetValue("ceilingUvScale", typeof(Vector2));
            this.FloorBottom = (float)info.GetValue("floorBottom", typeof(float));
            this.FloorShapeRotation = (float)info.GetValue("floorShapeRotation", typeof(float));
            this.FloorTop = (float)info.GetValue("floorTop", typeof(float));
            this.FloorUvOffset = (Vector2)info.GetValue("floorUvOffset", typeof(Vector2));
            this.FloorUvScale = (Vector2)info.GetValue("floorUvScale", typeof(Vector2));
            this.GridLocationX = (int)info.GetValue("locationX", typeof(int));
            this.GridLocationY = (int)info.GetValue("locationY", typeof(int));
            this.WallUvOffset = (Vector2)info.GetValue("wallUvOffset", typeof(Vector2));
            this.WallUvScale = (Vector2)info.GetValue("wallUvScale", typeof(Vector2));
        }

        #endregion

        #region Public Events

        /// <summary>
        /// Should be raised when material data changes.
        /// </summary>
        public event EventHandler MaterialChanged;

        /// <summary>
        /// Occurs when a property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs when a property is about to change.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Should be raised when vertex data changes.
        /// </summary>
        public event EventHandler VertexDataChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the bounds for the block model.
        /// </summary>
        [XmlIgnore]
        public Bounds Bounds
        {
            get
            {
                return this.bounds;
            }

            set
            {
                // does nothing
            }
        }

        /// <summary>
        /// Gets or sets the ceiling material.
        /// </summary>                        
        [XmlIgnore]
        public Material Ceiling
        {
            get
            {
                return this.ceiling;
            }

            set
            {
                if (value == this.ceiling)
                {
                    return;
                }

                this.OnPropertyChanging("Ceiling");
                this.ceiling = value;
                this.OnPropertyChanged("Ceiling");
                this.OnMaterialChanged();
            }
        }

        /// <summary>
        /// Gets or sets the ceiling shape.
        /// </summary>      
        [XmlIgnore]
        public GameObject CeilingShape
        {
            get
            {
                return this.ceilingShape;
            }

            set
            {
                if (value == this.ceilingShape)
                {
                    return;
                }

                this.OnPropertyChanging("CeilingShape");
                this.ceilingShape = value;
                this.OnPropertyChanged("CeilingShape");
            }
        }

        /// <summary>
        /// Gets or sets the ceiling shape rotation value.
        /// </summary>                     
        public float CeilingShapeRotation
        {
            get
            {
                return this.ceilingShapeRotation;
            }

            set
            {
                if (value == this.ceilingShapeRotation)
                {
                    return;
                }

                this.OnPropertyChanging("CeilingShapeRotation");
                this.ceilingShapeRotation = value;
                this.OnPropertyChanged("CeilingShapeRotation");
            }
        }

        /// <summary>
        /// Gets or sets the ceiling uv offset.
        /// </summary>    
        public Vector2 CeilingUvOffset
        {
            get
            {
                return this.ceilingUvOffset;
            }

            set
            {
                if (value == this.ceilingUvOffset)
                {
                    return;
                }

                this.OnPropertyChanging("CeilingUvOffset");
                this.ceilingUvOffset = value;
                this.OnPropertyChanged("CeilingUvOffset");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the ceiling uv scale.
        /// </summary>             
        public Vector2 CeilingUvScale
        {
            get
            {
                return this.ceilingUvScale;
            }

            set
            {
                if (value == this.ceilingUvScale)
                {
                    return;
                }

                this.OnPropertyChanging("CeilingUvScale");
                this.ceilingUvScale = value;
                this.OnPropertyChanged("CeilingUvScale");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the floor material.
        /// </summary>                      
        [XmlIgnore]
        public Material Floor
        {
            get
            {
                return this.floor;
            }

            set
            {
                if (value == this.floor)
                {
                    return;
                }

                this.OnPropertyChanging("Floor");
                this.floor = value;
                this.OnPropertyChanged("Floor");
                this.OnMaterialChanged();
            }
        }

        /// <summary>
        /// Gets or sets the floor bottom value.
        /// </summary>
        public float FloorBottom
        {
            get
            {
                return this.floorBottom;
            }

            set
            {
                if (value == this.floorBottom)
                {
                    return;
                }

                var changed = this.floorBottom != value;
                if (!changed)
                {
                    return;
                }

                this.OnPropertyChanging("FloorBottom");
                this.floorBottom = value;
                this.CorrectTopBottomValues();
                this.UpdateBounds();
                this.OnPropertyChanged("FloorBottom");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the floor shape.
        /// </summary>   
        [XmlIgnore]
        public GameObject FloorShape
        {
            get
            {
                return this.floorShape;
            }

            set
            {
                if (value == this.floorShape)
                {
                    return;
                }

                this.OnPropertyChanging("FloorShape");
                this.floorShape = value;
                this.OnPropertyChanged("FloorShape");
            }
        }

        /// <summary>
        /// Gets or sets the floor shape rotation value.
        /// </summary>           
        public float FloorShapeRotation
        {
            get
            {
                return this.floorShapeRotation;
            }

            set
            {
                if (value == this.floorShapeRotation)
                {
                    return;
                }

                this.OnPropertyChanging("FloorShapeRotation");
                this.floorShapeRotation = value;
                this.OnPropertyChanged("FloorShapeRotation");
            }
        }

        /// <summary>
        /// Gets or sets the floor top value.
        /// </summary>                      
        public float FloorTop
        {
            get
            {
                return this.floorTop;
            }

            set
            {
                if (value == this.floorTop)
                {
                    return;
                }

                var changed = this.floorTop != value;
                if (!changed)
                {
                    return;
                }

                this.OnPropertyChanging("FloorTop");
                this.floorTop = value;
                this.CorrectTopBottomValues();
                this.UpdateBounds();
                this.OnPropertyChanged("FloorTop");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the floor texture path.
        /// </summary>                      
        public string FloorTexturePath
        {
            get
            {
                return this.floorTexturePath;
            }

            set
            {
                if (value == this.floorTexturePath)
                {
                    return;
                }

                var changed = this.floorTexturePath != value;
                if (!changed)
                {
                    return;
                }

                this.OnPropertyChanging("FloorTexturePath");
                this.floorTexturePath = value;
                this.OnPropertyChanged("FloorTexturePath");    
            }
        }

        /// <summary>
        /// Gets or sets the wall texture path.
        /// </summary>                      
        public string WallTexturePath
        {
            get
            {
                return this.wallTexturePath;
            }

            set
            {
                if (value == this.wallTexturePath)
                {
                    return;
                }

                var changed = this.wallTexturePath != value;
                if (!changed)
                {
                    return;
                }

                this.OnPropertyChanging("WallTexturePath");
                this.wallTexturePath = value;
                this.OnPropertyChanged("WallTexturePath");
            }
        }

        /// <summary>
        /// Gets or sets the ceiling texture path.
        /// </summary>                      
        public string CeilingTexturePath
        {
            get
            {
                return this.ceilingTexturePath;
            }

            set
            {
                if (value == this.ceilingTexturePath)
                {
                    return;
                }

                var changed = this.ceilingTexturePath != value;
                if (!changed)
                {
                    return;
                }

                this.OnPropertyChanging("CeilingTexturePath");
                this.ceilingTexturePath = value;
                this.OnPropertyChanged("CeilingTexturePath");
            }
        }

        /// <summary>
        /// Gets or sets the floor uv offset.
        /// </summary>     
        public Vector2 FloorUvOffset
        {
            get
            {
                return this.floorUvOffset;
            }

            set
            {
                if (value == this.floorUvOffset)
                {
                    return;
                }

                this.OnPropertyChanging("FloorUvOffset");
                this.floorUvOffset = value;
                this.OnPropertyChanged("FloorUvOffset");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the floor uv scale.
        /// </summary>          
        public Vector2 FloorUvScale
        {
            get
            {
                return this.floorUvScale;
            }

            set
            {
                if (value == this.floorUvScale)
                {
                    return;
                }

                this.OnPropertyChanging("FloorUvScale");
                this.floorUvScale = value;
                this.OnPropertyChanged("FloorUvScale");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>    
        /// <remarks>This modifies the <see cref="FloorTop"/> property.</remarks>
        public float Height
        {
            get
            {
                return this.floorTop - this.floorBottom;
            }

            set
            {
                this.FloorTop = this.floorBottom + value;
            }
        }

        /// <summary>
        /// Gets or sets the x location within the map.
        /// </summary>
        public int GridLocationX
        {
            get
            {
                return this.gridLocationX;
            }

            set
            {
                if (value == this.gridLocationX)
                {
                    return;
                }

                this.OnPropertyChanging("GridLocationX");
                this.gridLocationX = value;

                // this.properties["LocationX"] = this.locationX;
                this.UpdateBounds();
                this.OnPropertyChanged("GridLocationX");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the y location within the map.
        /// </summary>
        public int GridLocationY
        {
            get
            {
                return this.gridLocationY;
            }

            set
            {
                if (value == this.gridLocationY)
                {
                    return;
                }

                this.OnPropertyChanging("GridLocationY");
                this.gridLocationY = value;

                // this.properties["LocationY"] = this.locationY;
                this.UpdateBounds();
                this.OnPropertyChanged("GridLocationY");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets the material array for the block model data.
        /// </summary>
        [XmlIgnore]
        public Material[] Materials
        {
            get
            {
                return new[] { this.floor, this.walls, this.ceiling };
            }
        }

        /// <summary>
        /// Gets the normals for the block model.
        /// </summary>
        public Vector3[] Normals
        {
            get
            {
                return this.vertexNormals;
            }
        }

        /// <summary>
        /// Gets the reserved vertex count.
        /// </summary>
        /// <remarks>
        ///   <p>If the mesh is intended to change vertex counts you can set reserved vertex count to the max number of vertexes needed. </p> 
        /// <p>This will help optimize the mesh when adding, updating and removing.</p>
        /// </remarks>
        public int ReservedVertexCount
        {
            get
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets the number of sub meshes for the block model.
        /// </summary>
        public int SubMeshCount
        {
            get
            {
                return 3;
            }
        }

        /// <summary>
        /// Gets the texture coordinates for the block model.
        /// </summary>
        public Vector2[] TextureCoordinates
        {
            get
            {
                // pre-calculate wall uvs
                var rightX = (1f * this.wallUvScale.x) - this.wallUvOffset.x;
                var leftX = (0f * this.wallUvScale.x) - this.wallUvOffset.x;
                var topY = (0f * this.wallUvScale.y) - this.wallUvOffset.y;
                var bottomY = (1f * this.wallUvScale.y) - this.wallUvOffset.y;
                var uvs = new[]
                              {
                                  new Vector2(
                                      (0f * this.ceilingUvScale.x) - this.ceilingUvOffset.x, 
                                      (0f * this.ceilingUvScale.y) - this.ceilingUvOffset.y), // 0 bottom
                                  new Vector2(
                                      (1f * this.ceilingUvScale.x) - this.ceilingUvOffset.x, 
                                      (0f * this.ceilingUvScale.y) - this.ceilingUvOffset.y), // 1 bottom
                                  new Vector2(
                                      (1f * this.ceilingUvScale.x) - this.ceilingUvOffset.x, 
                                      (1f * this.ceilingUvScale.y) - this.ceilingUvOffset.y), // 2 bottom
                                  new Vector2(
                                      (0f * this.ceilingUvScale.x) - this.ceilingUvOffset.x, 
                                      (1f * this.ceilingUvScale.y) - this.ceilingUvOffset.y), // 3 bottom
                        
                                  new Vector2(
                                      (1f * this.floorUvScale.x) - this.floorUvOffset.x, 
                                      (0f * this.floorUvScale.y) - this.floorUvOffset.y), // 4 top
                                  new Vector2(
                                      (0f * this.floorUvScale.x) - this.floorUvOffset.x, 
                                      (0f * this.floorUvScale.y) - this.floorUvOffset.y), // 5 top
                                  new Vector2(
                                      (0f * this.floorUvScale.x) - this.floorUvOffset.x, 
                                      (1f * this.floorUvScale.y) - this.floorUvOffset.y), // 6 top
                                  new Vector2(
                                      (1f * this.floorUvScale.x) - this.floorUvOffset.x, 
                                      (1f * this.floorUvScale.y) - this.floorUvOffset.y), // 7 top
                                                                                                               
                                  new Vector2(rightX, topY), // 8 back
                                  new Vector2(leftX, bottomY), // 9 back
                                  new Vector2(leftX, topY), // 10 back
                                  new Vector2(rightX, bottomY), // 11 back
                                                                                                               
                                  new Vector2(rightX, topY), // 12 left
                                  new Vector2(leftX, bottomY), // 13 left
                                  new Vector2(leftX, topY), // 14 left
                                  new Vector2(rightX, bottomY), // 15 left
                                                                                                               
                                  new Vector2(rightX, topY), // 16 front
                                  new Vector2(leftX, bottomY), // 17 front
                                  new Vector2(leftX, topY), // 18 front
                                  new Vector2(rightX, bottomY), // 19 front
                                                                                                               
                                  new Vector2(rightX, topY), // 20 right
                                  new Vector2(leftX, bottomY), // 21 right
                                  new Vector2(leftX, topY), // 22 right
                                  new Vector2(rightX, bottomY) // 23 right   
                              };

                return uvs;
            }
        }

        /// <summary>
        /// Gets the total triangle count.
        /// </summary>
        public int TriangleCount
        {
            get
            {
                return 12;
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier for this block model instance.
        /// </value>
        public Guid UniqueId
        {
            get
            {
                return this.instanceID;
            }
        }

        /// <summary>
        /// Gets the number of vertexes for the block model.
        /// </summary>
        public int VertexCount
        {
            get
            {
                return 24;
            }
        }

        /// <summary>
        /// Gets the vertexes for this block model instance.
        /// </summary>
        public Vector3[] Vertexes
        {
            get
            {
                var vertexes = new[] {
                    new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), 
                                       
                                       // 0 bottom 
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f), 
                                       
                                       // 1 bottom
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), 
                                       
                                       // 2 bottom
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), 
                                       
                                       // 3 bottom
                                                                                  
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), // 4 top
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f), // 5 top
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), // 6 top
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), // 7 top
                                                                                   
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), 
                                       
                                       // 8 back
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), // 9 back
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), 
                                       
                                       // 10 back
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), // 11 back
                                                                                               
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), 
                                       
                                       // 12 left
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), // 13 left
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), 
                                       
                                       // 14 left
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, -0.5f), // 15 left
                                                                                   
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), 
                                       
                                       // 16 front
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f), // 17 front 
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f), 
                                       
                                       // 18 front 
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(-0.5f, 0, 0.5f), // 19 front
                                                                                  
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f), 
                                       
                                       // 20 right
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), // 21 right
                                       new Vector3(this.gridLocationX, this.floorBottom, this.gridLocationY) + new Vector3(0.5f, 0, -0.5f), 
                                       
                                       // 22 right
                                       new Vector3(this.gridLocationX, this.floorTop, this.gridLocationY) + new Vector3(0.5f, 0, 0.5f) // 23 right
                                     };

                return vertexes;
            }
        }

        /// <summary>
        /// Gets or sets the wall uv offset.
        /// </summary>       
        public Vector2 WallUvOffset
        {
            get
            {
                return this.wallUvOffset;
            }

            set
            {
                if (value == this.wallUvOffset)
                {
                    return;
                }

                this.OnPropertyChanging("WallUvOffset");
                this.wallUvOffset = value;
                this.OnPropertyChanged("WallUvOffset");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the wall uv scale.
        /// </summary>       
        public Vector2 WallUvScale
        {
            get
            {
                return this.wallUvScale;
            }

            set
            {
                if (value == this.wallUvScale)
                {
                    return;
                }

                this.OnPropertyChanging("WallUvScale");
                this.wallUvScale = value;
                this.OnPropertyChanged("WallUvScale");
                this.OnVertexDataChanged();
            }
        }

        /// <summary>
        /// Gets or sets the wall material.
        /// </summary>    
        [XmlIgnore]
        public Material Walls
        {
            get
            {
                return this.walls;
            }

            set
            {
                if (value == this.walls)
                {
                    return;
                }

                this.OnPropertyChanging("Walls");
                this.walls = value;
                this.OnPropertyChanged("Walls");
                this.OnMaterialChanged();
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets triangle indices for a specific sub mesh.
        /// </summary>
        /// <param name="subMeshIndex">
        /// The index of the sub mesh to retrieve indicies data from.
        /// </param>
        /// <returns>
        /// An array of vertex indexes representing a series of triangles.
        /// </returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// If 'subMeshIndex' is less then 0 or greater then 2.
        /// </exception>
        public int[] GetIndicies(int subMeshIndex)
        {
            int[] indexes = null;
            if (subMeshIndex < 0 || subMeshIndex > 2)
            {
                throw new ArgumentOutOfRangeException("subMeshIndex");
            }

            switch (subMeshIndex)
            {
                case 0:
                    indexes = new[]
                                  {
                                      4, 5, 6, // top
                                      6, 7, 4 // top
                                  };
                    break;

                case 1:
                    indexes = new[]
                                  {
                                      10, 9, 8, // back
                                      11, 8, 9, // back
                                    
                                      14, 13, 12, // left
                                      15, 12, 13, // left
                                          
                                      18, 17, 16, // front
                                      19, 16, 17, // front
                                          
                                      22, 21, 20, // right
                                      23, 20, 21 // right
                                  };
                    break;

                case 2:
                    indexes = new[]
                                  {
                                      1, 0, 3, // bottom
                                      3, 2, 1 // bottom
                                  };
                    break;
            }

            return indexes;
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo"/> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">
        /// The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> to populate with data.
        /// </param>
        /// <param name="context">
        /// The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext"/>) for this serialization.
        /// </param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            // Use the AddValue method to specify serialized values.
            info.AddValue("ceilingShapeRotation", this.ceilingShapeRotation, typeof(float));
            info.AddValue("ceilingUvOffset", this.ceilingUvOffset, typeof(Vector2));
            info.AddValue("ceilingUvScale", this.ceilingUvScale, typeof(Vector2));
            info.AddValue("floorBottom", this.floorBottom, typeof(float));
            info.AddValue("floorShapeRotation", this.floorShapeRotation, typeof(float));
            info.AddValue("floorTop", this.floorTop, typeof(float));
            info.AddValue("floorUvOffset", this.floorUvOffset, typeof(Vector2));
            info.AddValue("floorUvScale", this.floorUvScale, typeof(Vector2));
            info.AddValue("locationX", this.gridLocationX, typeof(int));
            info.AddValue("locationY", this.gridLocationY, typeof(int));
            info.AddValue("wallUvOffset", this.wallUvOffset, typeof(Vector2));
            info.AddValue("wallUvScale", this.wallUvScale, typeof(Vector2));
        }

        /// <summary>
        /// Sets the ceiling uv offset and scale values.
        /// </summary>
        /// <param name="offsetU">
        /// The offset u.
        /// </param>
        /// <param name="offsetV">
        /// The offset v.
        /// </param>
        /// <param name="scaleU">
        /// The scale u.
        /// </param>
        /// <param name="scaleV">
        /// The scale v.
        /// </param>
        public void SetCeilingUv(float offsetU, float offsetV, float scaleU, float scaleV)
        {
            this.ceilingUvOffset = new Vector2(offsetU, offsetV);
            this.ceilingUvScale = new Vector2(scaleU, scaleV);
        }

        /// <summary>
        /// Sets the floor uv offset and scale values.
        /// </summary>
        /// <param name="offsetU">
        /// The offset u.
        /// </param>
        /// <param name="offsetV">
        /// The offset v.
        /// </param>
        /// <param name="scaleU">
        /// The scale u.
        /// </param>
        /// <param name="scaleV">
        /// The scale v.
        /// </param>
        public void SetFloorUv(float offsetU, float offsetV, float scaleU, float scaleV)
        {
            this.floorUvOffset = new Vector2(offsetU, offsetV);
            this.floorUvScale = new Vector2(scaleU, scaleV);
        }

        /// <summary>
        /// Sets the materials for the model.
        /// </summary>
        /// <param name="top">
        /// The top material.
        /// </param>
        /// <param name="wall">
        /// The wall material.
        /// </param>
        /// <param name="bottom">
        /// The bottom material.
        /// </param>
        public void SetMaterials(Material top, Material wall, Material bottom)
        {
            var changedFloor = top != this.floor;
            var changedWalls = wall != this.walls;
            var changedCeiling = bottom != this.ceiling;

            if (!changedFloor && !changedWalls && !changedCeiling)
            {
                return;
            }

            if (changedFloor)
            {
                this.OnPropertyChanging("Floor");
            }

            if (changedWalls)
            {
                this.OnPropertyChanging("Walls");
            }

            if (changedCeiling)
            {
                this.OnPropertyChanging("Ceiling");
            }

            this.ceiling = bottom;
            this.floor = top;
            this.walls = wall;

            if (changedFloor)
            {
                this.OnPropertyChanged("Floor");
            }

            if (changedWalls)
            {
                this.OnPropertyChanged("Walls");
            }

            if (changedCeiling)
            {
                this.OnPropertyChanged("Ceiling");
            }

            this.OnMaterialChanged();
        }

        /// <summary>
        /// Sets the floor and ceiling properties.
        /// </summary>
        /// <param name="top">
        /// The top value.
        /// </param>
        /// <param name="bottom">
        /// The bottom value.
        /// </param>
        public void SetProperties(float top, float bottom)
        {
            var changedTop = top != this.floorTop;
            var changedBottom = bottom != this.floorBottom;

            if (!changedBottom && !changedTop)
            {
                return;
            }

            if (changedTop)
            {
                this.OnPropertyChanging("FloorTop");
            }

            if (changedBottom)
            {
                this.OnPropertyChanging("FloorBottom");
            }

            this.floorTop = top;
            this.floorBottom = bottom;
            this.CorrectTopBottomValues();
            this.UpdateBounds();

            if (changedTop)
            {
                this.OnPropertyChanged("FloorTop");
            }

            if (changedBottom)
            {
                this.OnPropertyChanged("FloorBottom");
            }

            this.OnVertexDataChanged();
        }

        /// <summary>
        /// Sets the wall uv offset and scale values.
        /// </summary>
        /// <param name="offsetU">
        /// The offset u.
        /// </param>
        /// <param name="offsetV">
        /// The offset v.
        /// </param>
        /// <param name="scaleU">
        /// The scale u.
        /// </param>
        /// <param name="scaleV">
        /// The scale v.
        /// </param>
        public void SetWallUv(float offsetU, float offsetV, float scaleU, float scaleV)
        {
            this.wallUvOffset = new Vector2(offsetU, offsetV);
            this.wallUvScale = new Vector2(scaleU, scaleV);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the <see cref="MaterialChanged"/> event.
        /// </summary>
        protected virtual void OnMaterialChanged()
        {
            var handler = this.MaterialChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="propertyName">
        /// Name of the property that changed.
        /// </param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                PropertyChangedEventArgs args;
                if (this.changedArgs.ContainsKey(propertyName))
                {
                    args = this.changedArgs[propertyName];
                }
                else
                {
                    args = new PropertyChangedEventArgs(propertyName);
                    this.changedArgs.Add(propertyName, args);
                }

                handler(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanging"/> event.
        /// </summary>
        /// <param name="propertyName">
        /// Name of the property that is changing.
        /// </param>
        protected virtual void OnPropertyChanging(string propertyName)
        {
            var handler = this.PropertyChanging;
            if (handler != null)
            {
                PropertyChangingEventArgs args;
                if (this.changingArgs.ContainsKey(propertyName))
                {
                    args = this.changingArgs[propertyName];
                }
                else
                {
                    args = new PropertyChangingEventArgs(propertyName);
                    this.changingArgs.Add(propertyName, args);
                }

                handler(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="VertexDataChanged"/> event.
        /// </summary>
        protected virtual void OnVertexDataChanged()
        {
            var handler = this.VertexDataChanged;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Corrects the top bottom values so they do not cross over each other.
        /// </summary>
        private void CorrectTopBottomValues()
        {
            // var originalTop = this.floorTop;
            // var originalBottom = this.floorBottom;
            this.floorTop = this.floorTop > 15 ? 15 : this.floorTop;
            this.floorTop = this.floorTop < -15 ? -15 : this.floorTop;

            this.floorBottom = this.floorBottom > 15 ? 15 : this.floorBottom;
            this.floorBottom = this.floorBottom < -15 ? -15 : this.floorBottom;

            this.floorTop = this.floorTop < this.floorBottom ? this.floorBottom : this.floorTop;
            this.floorBottom = this.floorBottom > this.floorTop ? this.floorTop : this.floorBottom;

            // if (originalTop != this.floorTop || originalBottom != this.floorBottom)
            // {
            // this.OnVertexDataChanged();
            // }
        }

        /// <summary>
        /// Updates the bounds of the model.
        /// </summary>
        private void UpdateBounds()
        {
            this.bounds.center = new Vector3(this.gridLocationX, this.floorBottom + (this.Height / 2f), this.gridLocationY);
            this.bounds.size = new Vector3(1, this.Height, 1);
        }

        #endregion                   
    }
}